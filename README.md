# MtG Goldfish data analysis
This repository is meant to host a collection of odds and ends of card data
analysis to support articles for the website MtG Goldfish.


# About

This repository is not a tutorial on programming, best-practices, etc.
Use this code at your own risk. For those using the codebase it is assumed you
have knowledge of best practices (e.g. virtual environments / conda for package
management).

This repository provides some Jupyter notebooks under the directory `jupyter`
to interactively play around with the code used. However, they are mainly for
supporting articles by utilizing a combination of markdown, minimal code, and plots.

These notebooks leverage a _local_ python library `mtgf`, which can be installed via:

```bash
cd path/to/repo
pip install -e ./
```

For the text analysis if you wish to use `textblob` you will need to run:

```python
import nltk
nltk.download('punkt')
nltk.download('wordnet')
```

and if you wish to use `spacy`, from the command line run:

```bash
$ python -m spacy download en
```
