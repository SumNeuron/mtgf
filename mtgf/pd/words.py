import pandas as pd, numpy as np
from mtgf.pd.defaults import WORDS_TO_IGNORE, WORDS_OF_IMPORTANCE
from textblob import TextBlob
import spacy

def unique_words_from_oracle_text(
    text:str,
    library:str='spacy',
    words_to_ignore:list=None,
    words_of_importance:list=None,
):
    '''
    Give a string of text, finds the unique words within by utilizing
    "lemmatization".

    Arguments:
        text (str): the oracle text of a card as provided by Scryfall.

        library (str): Either `'spacy'` or `'textblob'`. Which python library
            to use for lemmitization. Defaults to `'spacy'`.

        words_to_ignore (list): a list of words to ignore. By default set to
            `None`. If `None`, will use human currated defaults depending on
            the `library` provided. If `words_of_importance` is `None`, filters
             lemmatized words for words not in this list.

        words_of_importance (list): a list of words to count. By defualt `None`.
            If provided, will filter lemmatized words for words exclusively
            in this list.

    Returns:
        words (list): a list of unique words found in text.
    '''
    # simple error handling
    if type(text) is not str: return set([])

    # get defaults based on the library
    if words_to_ignore is None:
        words_to_ignore = WORDS_TO_IGNORE(library)

    # get lemmatized words by library
    if library == 'spacy':
        nlp = spacy.load('en', disable=['parser', 'ner'])
        doc = nlp(text.lower())
        words = [token.lemma_ for token in doc]

    if library == 'textblob':
        b = TextBlob(text.lower())
        words = b.words.lemmatize()

    # either look for words in or not in provided list
    if words_of_importance is not None:
        filter_fn = lambda w: w in words_of_importance
    else:
        filter_fn = lambda w: w not in words_to_ignore

    words = list(filter(filter_fn, list(words)))
    return set(words)

def unique_word_occurrences(
    df,
    library             :str   = 'spacy',
    words_to_ignore     :list  = None,
    words_of_importance :list  = None,
):
    '''
    Notes:
        - this counts the occurrence of words by each entry of the passes
            dataset e.g. if the first entry, `a`, has the word `"lifelink"`
            trice, it contributes a _value_ of `1` towards the count.
            In other words, the returned list of tuples are the total number of
            entries in the DataFrame that have the word.
    Arguments:
        df (pd.DataFrame): a pandas DataFrame on the list of Card Objects
            returned from the Scryfall API.

        library (str): Either `'spacy'` or `'textblob'`. Which python library
            to use for lemmitization. Defaults to `'spacy'`.

        words_to_ignore (list): a list of words to ignore. By default set to
            `None`. If `None`, will use human currated defaults depending on
            the `library` provided. If `words_of_importance` is `None`, filters
             lemmatized words for words not in this list.

        words_of_importance (list): a list of words to count. By defualt `None`.
            If provided, will filter lemmatized words for words exclusively
            in this list.

    Returns:
        tallies (list): a list of tuples `(word, count)`, that occur in `df`
    '''
    # tuples is a list of (card_name, {word_lemma1, word_lemma2, ...}) pairs
    tuples = list(zip(
        df.name,
        df.oracle_text.apply(
            lambda card: unique_words_from_oracle_text(
                card, library, words_to_ignore, words_of_importance
            )
        )
    ))

    # tally occurrences
    tallies = {}
    for name, words in tuples:
        for word in words:
            if word not in tallies:
                tallies[word] = 0
            tallies[word] += 1


    results = list(zip(tallies.keys(), tallies.values()))
    # sort by count, defaults to ascending
    results = sorted(results, key=lambda t: t[-1])
    # return largest first
    results.reverse()
    return results


def top_words_occurrences(
    df,
    threshold           :float = 0.,
    library             :str   = 'spacy',
    words_to_ignore     :list  = None,
    words_of_importance :list  = None,
):
    '''
    Notes:
        - lazy wrapper around `unique_word_occurrences`

        - simply filters output of `unique_word_occurrences` by whether or not
            the count in the `(word, count)` tuples are greater than
            `threshold * len(df)`.

    Arguments:
        df (pd.DataFrame): a pandas DataFrame on the list of Card Objects
            returned from the Scryfall API.

        threshold (float): The percent of entries a word must occur in order
            be pass the filter for "top" words. Defaults to `0.`.
            e.g. `count >= threshold * len(df)`

        library (str): Either `'spacy'` or `'textblob'`. Which python library
            to use for lemmitization. Defaults to `'spacy'`.

        words_to_ignore (list): a list of words to ignore. By default set to
            `None`. If `None`, will use human currated defaults depending on
            the `library` provided. If `words_of_importance` is `None`, filters
             lemmatized words for words not in this list.

        words_of_importance (list): a list of words to count. By defualt `None`.
            If provided, will filter lemmatized words for words exclusively
            in this list.

    Returns:
        tallies (list): a list of tuples `(word, count)`, that occur in `df`
    '''
    word_tallies = unique_word_occurrences(df, library, words_to_ignore, words_of_importance)
    top_words = list(filter(lambda t: t[-1]>=threshold*len(df), word_tallies))
    return top_words

def card_for_pivot_table(card, reference_words):
    '''
    Arguments:
        card (pd.DataFrame row): a row from the pandas DataFrame on the list of
            Card Objects returned from the Scryfall API. In other words, a
            card object itself.

        reference_words (list): a list of words to test occurence against.

    Returns:
        coordinates (list): a list of word occurences encoded in coordinate
        format e.g.
            ```python
            {
                name: name_of_card,
                word: word_of_interest,
                state: whether_word_in_card
            }
            ```
    '''
    cards_words = unique_words_from_oracle_text(card.oracle_text)
    res = []
    for word in reference_words:
        res.append({
            'name': card['name'],
            'word': word,
            'state': word in cards_words
        })
    return res

def to_word_coo(df, word_tallies):
    '''
    Notes:
        - helper function for subsequent functions like ploting heatmap.
    Arguments:
        df (pd.DataFrame): a pandas DataFrame on the list of Card Objects
            returned from the Scryfall API.
        word_tallies (list): a list of tuples `(word, count)`, that occur in `df`
    Returns:
        coo_df (pd.DataFrame): a pandas DataFrame in coordinate format
            containing three columns:
                - `name` (str): name of the card
                - `word` (str): what the word is
                - `state` (bool): whether the given card has the word in its
                    oracle text
    '''
    words = [word for word, count in word_tallies]
    prepivot = df.apply(
        lambda card: card_for_pivot_table(card, words),
        axis=1
    ).to_numpy()
    flattened = np.array([np.array(e) for e in prepivot]).flatten().tolist()
    return pd.DataFrame(flattened)


def word_density(df, word_tallies):
    '''
    Calculate the density of the boolean word occurence matrix.

    Arguments:
        df (pd.DataFrame): a pandas DataFrame on the list of Card Objects
            returned from the Scryfall API.
        word_tallies (list): a list of tuples `(word, count)`, that occur in `df`

    Returns:
        denisty (float): the density of the word occurence matrix
    '''
    pivot = pd.pivot_table(
        to_word_coo(df, word_tallies),
        values='state', index=['word'], columns=['name'], margins=True
    )
    n_words, n_planeswalkers = list(map(len, pivot.drop('All', 1).axes))
    n_cells = (n_words * n_planeswalkers)
    return pivot.drop('All', 1).sum().sum() / n_cells
