# the columns of the Pandas DataFrame which are relevant for Text Analysis
RELEVANT_COLUMNS = [
    'name', 'mana_cost', 'cmc', 'type_line', 'oracle_text', 'loyalty', 'colors',
    'color_identity', 'rarity', 'edhrec_rank'
]

def WORDS_TO_IGNORE(library='spacy'):
    words = TOP_100_COMMON_WORDS
    if library == 'spacy':
        words += SPACY_WORDS_THAT_APPEAR_ONCE
        words += SPACY_PUNCTUATION_TO_REMOVE
        words += SPACY_WORDS_OTHER_TO_IGNORE
    if library == 'textblob':
        words += TEXTBLOB_WORDS_THAT_APPEAR_ONCE
        words += TEXTBLOB_PUNCTUATION_TO_REMOVE
        words += TEXTBLOB_WORDS_ARTIFACTS
        words += TEXTBLOB_WORDS_OTHER_TO_IGNORE
    return words

'''
What follows are seven lists of words which are used in the function above
`WORDS_TO_IGNORE`. Variables which start with 'SPACY' refer to values meant
to be used with the `spacy` library, and those that start with 'TEXTBLOB' for
the `textblob` library. These exists due to differences in lemmatization and
various artifacts these parsers may produce.

- `TOP_100_COMMON_WORDS`: these are the top 100 common words in english and
    likely have no relevance to the information we are trying to extract.
    Notably, the word `more`, has been removed from this list.

- `<library>_PUNCTUATION_TO_REMOVE`: this is punctuation which will make our
    word extractor work better once removed.

- `<library>_WORDS_THAT_APPEAR_ONCE`: these words occur on only one planeswalker
    card with the query for all planeswalkers, e.g. 'type:planeswalker'.
    So no harm in removing for brevity in our word matrix.

- `<library>_WORDS_ARTIFACTS`: artifacts from the lemmitization which should be
    ignored for better results.

- `<library>_WORDS_OTHER_TO_IGNORE`: Like above, these are words from all the
    Planeswalkers. I am personally letting them be removed because they do not
    seem relevant to what we are trying to find.

'''

TEXTBLOB_PUNCTUATION_TO_REMOVE = list('.,;:(){}')+['"', "'", '\n']

TOP_100_COMMON_WORDS = [
    'a', 'about', 'all', 'also', 'and', 'as', 'at', 'be', 'because', 'but',
    'by', 'can', 'come', 'could', 'day', 'do', 'even', 'find', 'first', 'for',
    'from', 'get', 'give', 'go', 'have', 'he', 'her', 'here', 'him', 'his',
    'how', 'I', 'if', 'in', 'into', 'it', 'its', 'just', 'know', 'like', 'look',
    'make',  'man', 'many', 'me', 'my', 'new', 'no', 'not', 'now', 'of', 'on',
    'one', 'only', 'or', 'other', 'our', 'out', 'people', 'say', 'see', 'she',
    'so', 'some', 'take', 'tell', 'than', 'that', 'the', 'their', 'them',
    'then', 'there', 'these', 'they', 'thing', 'think', 'this', 'those', 'time',
    'to', 'two', 'up', 'use', 'very', 'want', 'way', 'we', 'well', 'what',
    'when', 'which', 'who', 'will', 'with', 'would', 'year', 'you', 'your'
]


TEXTBLOB_WORDS_THAT_APPEAR_ONCE = [
    'satyr', 'discarding', 'wrenn', 'vraska', 'unseen', '10/+10', 'remains',
    'click', 'askurza.com', 'ugin', 'leaf', 'fiend-blooded', 'tibalt',
    'etherium', 'cell', 'tezzeret', 'master', 'bridge', 'twice', 'archmage',
    'temporal', 'teferi', 'third', 'size', 'anywhere', 'maximum', 'either',
    'vengeful', 'markov', 'imperious', 'grim', 'highest', 'nemesis', 'reduce',
    'reduces', 'creates', 'mad', 'himself', 'dragonspeaker', 'dragonsoul',
    '2/+1', 'tested', 'samut', 'gifted', 'rai', 'different', 'zarek', 'conduit',
    'viceroy', 'izzet', 'caller', 'hate-twisted', 'oath', 'lost', 'sacrificed',
    '0/1', 'steward', 'element', 'deceiver', 'unless', 'god-pharaoh',
    'dragon-god', 'rebound', 'resolve', 'equipped', 'kor', 'stoneforged',
    'lithomancer', 'blade', 'attach', 'stone', '2/-0', '5/-0', 'windgrace',
    '2/-1', '1/-1', 'non-zombie', '9/9', '8/8', 'usurper', 'orzhov', 'owns',
    'single', 'did', 'silver', 'leaving', 'liberated', 'karn', '−14', 'mowu',
    'venerated', 'firemage', 'jaya', 'cunning', 'castaway', 'jace', '1/-0',
    'second', 'poet', 'rather', 'assigns', 'radiant', 'non-gideon',
    'oathsworn', 'trial', 'martial', 'paragon', 'jura', 'justice',
    '−15', 'blackblade', 'zendikar', 'llanowar', 'freyalise', 'enchant', 'armor',
    'masked', 'same', 'estrid', 'enchanted', 'attached', 'dovin', 'grand',
    'arbiter', '3/-0', 'skip', 'domri', 'city', 'smasher', 'spent', 'riot',
    'fewer', 'shadowmage', 'davriel', 'rogue', 'savant', 'daretti', 'scrap',
    'torch', 'defiance', '5', 'firebrand', 'pyromaster', 'pyrogenius',
    'elementals', 'novice', 'nalaar', 'flame', 'flamecaller', '3/1', 'artisan',
    'removed', 'fire', 'bold', 'inferno', 'awakened', 'die', 'non-elemental',
    'ablaze', 'weaver', 'ashiok', 'nightmare', 'angrath', 'controlled', 'left',
    'fateshifter', 'right', 'aminatou', 'vengeant', 'pridemate', 'starting',
    'pride', '15', '100',
]

TEXTBLOB_WORDS_ARTIFACTS = [
    'ymy', 'thi', "'", 'plu', 'fmy', 'plu', 'remain', 'teferus', 'imperiou',
    'nemesi', 'unles', 'octopu', 'assign', 'domrus', 'darettus', 'pyrogeniu',
    'amas'
]

TEXTBLOB_WORDS_OTHER_TO_IGNORE = [
    '1', 'turn', 'an', '2', '−2', 'deal', "'s", '−3', 'create', 'opponent',
    'player', 'top', 'emblem', 'may', 'any', 'next',  '−7', '−8', "n't", '−1',
    'three', 'return', 'onto', 'x', 'is', 'reveal', 'beginning', '−6', 'step',
    '0',  'among', 'ca', 'rest', '1/1',  '3', 'order', '−10', '−9', 'where',
    '2/2', 'le', '5/5', 'still', 'dealt', '4', 'chandra', '4/4', 'ability',
    'commander', '−x', 'four', 'seven', 'another', 'during', '−4', 't', '−5',
    'r', 'ha', 'owner', 'gideon',  'type', 'addition', 'and/or', 'without',
    'own', 'named', 'sorin', '7', 'long', '5/+5', '3/3', 'g', 'ten', '2/+2',
    'doe', 'five', 'planeswalkers', 'base', 'ral', 'bola', 'nicol', 'legendary',
    "'re", 'except', '1/+0', '10', '−11', 'are',  'sarkhan', 'activate',
    'after', 'x/-x', 'though', '−12', 'divided', 'x/+x', 'greatest', 'greater',
    'choice', 'six', '6', 'much', '0/0', 'chosen', 'least', '0/3', 'had',
    'bloodlord', 'combination', 'name', 'saheeli', 'partner', '8', 'rowan',
    'kenrith', 'head', 'ob', 'nixilis', 'nissa', 'nahiri', 'lord', 'b', 'dy',
    'kaya', 'chooses', 'while', 'twenty', 'separate', 'huatli', '4/+4', 'wa',
    'able', 'champion', '6/6',   '3/+3', 'activated', 'construct', '2/+0',
    'fury', 'pyromancer', 'countered', 'discarded', 'cause', 'revealed',
    'ajani',
]


SPACY_PUNCTUATION_TO_REMOVE = [
    '.', '\n', ':', '-PRON-', ',', '"', '{', '}', '(', ')', "'"
]

SPACY_WORDS_THAT_APPEAR_ONCE = [
    'g', 'wrenn', 'vraska', 'unseen', 'remain', 'ugin', 'tibalt', 'fiend',
    'blooded', 'etherium', 'x/-x', 'cell', 'master', 'tezzeret', 'bridge',
    'twice', 'archmage', 'teferi', 'temporal', 'third', 'maximum', 'size',
    'anywhere', 'either', 'vengeful', 'markov', 'imperious', 'grim', 'nemesis',
    'high', 'reduce', 'mad', 'dragonspeaker', '+2/+1', 'samut', 'test',
    'different', 'rai', 'saheeli', 'come', 'head', 'zarek', 'conduit',
    'viceroy', 'izzet', 'hate', 'twist', '1}{b', 'oath', 'sacrificed', '0/1',
    'steward', 'element', '+3', 'pharaoh', 'who', '0}.', 'stoneforge',
    'equipped', 'kor', 'lithomancer', 'blade', 'stone', '-2/-0', 'windgrace', '−11',
    'lord', '-2/-1', 'b}{b}{b}{b}.', '9/9', '8/8', 'single', 'orzhov', 'usurper',
    'silver', 'liberate', 'karn', '−14', '+4', 'mowu', 'venerate', 'jaya',
    'firemage', 'r}{r}{r}.', 'cunne', 'castaway', 'jace', '-1/-0', 'poet',
    'rather', 'radiant', 'trial', 'jura', '−15', 'justice', 'blackblade',
    'zendikar', 'fury', 'llanowar', 'freyalise', 'armor', 'same', 'enchant',
    'estrid', 'grand', 'arbiter', 'dovin', '-3/-0', 'activated', 'davriel',
    'few', 'shadowmage', 'rogue', 'scrap', 'savant', 'daretti', 'defiance', '5',
    'torch', '6', 'firebrand', 'pyromaster', 'pyromancer', '+2/+0', 'novice',
    'nalaar', 'flamecaller', '3/1', 'artisan', 'remove', 'fire', 'inferno',
    'awaken', 'dealt', 'ablaze', 'nightmare', 'weaver', 'ashiok', 'convert',
    'right', 'fateshifter', 'aminatou', 'vengeant', 'pridemate', '15', 'start',
    'pride', 'but', '100',
]

SPACY_WORDS_OTHER_TO_IGNORE = [
    'a', 'of', 'the', 'to', '+1', 'with', 'and', 'an', 'be', 'that', '−2', 'or',
    'until', 'end', 'emblem', 'opponent', 'may', 'from', 'player', 'top',
    'into', 'this', 'one', '−3', 'two', 'at', 'then', 'on', 'any', 'up', 'next',
    'if', 'in', 'have', '+2', 'all', 'for', "'s", '−8', '−7', 'can', '1', 'not',
    '−1', '{', 'x', 'three', '2', 'other', 'beginning', '−6', 'step', '}',
    'do', '0', '1/1', 'would', 'look', 'among', '2/2', 'where', 'converted',
    '4/4', 'ability', 'under', 'way', 'more', 'order', 'fly', 'as', 'still',
    '−x', 'commander', 'controller', '3', 'four', '5/5', 'chandra', 'another',
    '−5', '-', '−10', '(', ')', 'during', 'name', '−4', 't', 'when', 'seven',
    '4', 'own', '−9', 'than', 'owner', 'great', 'without', 'addition', 'type',
    'those', 'long', 'gideon', '3/3', 'activate', 'and/or', 'five', 'base',
    'sorin', 'legendary', '+', 'except', 'r', 'g}.', 'new', 'ten', 'non', "'",
    'by', 'plus', 'ral', '7', 'nicol', 'bola', 'though', 'only', 'x/+x',
    '+2/+2', 'choice', '+1/+0', 'six', 'time', 'many', 'much', '0/0', 'leave',
    'least', '0/3', 'could', 'bloodlord', 'sarkhan', 'combination', '-2',
    'kenrith', 'will', 'rowan', 'partner', '-8', 'ob', 'nixilis', 'nissa',
    'god', 'nahiri', 'after', '-x/-x', 'kaya', 'no', 'while', '−12', 'twenty',
    'separate', 'huatli', 'able', 'champion', '6/6', '+5/+5', 'attach', '+3/+3',
    'r}{r}.', '10', 'cause', 'ajani',
]

WORDS_OF_IMPORTANCE = [
    # colors
    'red', 'white', 'green', 'color', 'colorless', 'black', 'blue',

    # types
    'sorcery', 'instant', 'enchantment', 'noncreature', 'land', 'nonland',
    'aura', 'artifact', 'creature', 'permanent', 'spell', 'basic', 'planeswalker',
    'equipment',
    # tribes
    'token',
    'swamp', 'mountain', 'forest', 'island',
    'elemental', 'soldier', 'spirit',  'knight', 'elf', 'thopter',
    'construct', 'dragon', 'cat', 'zombie', 'vampire', 'human', 'beast',
    'assassin', 'warrior', 'defender', 'satyr', 'pirate', 'treasure', 'devil', 'wall',
    'angel', 'servo', 'demon', 'plant', 'bird', 'octopus',
    'ghost', 'wizard', 'hound', 'illusion', 'dinosaur', 'avatar',
    'ally', 'wurm', 'druid', 'mask', 'army', 'werewolf', 'wolf',

    # effects
    'search', # tutor
    'lose', 'win', 'game', 'restart', # end the game
    'less', 'spend', 'pay', 'add', 'cost', 'more', # ramp / tax
    'play', 'cast', 'copy',
    'face', 'down', # morph
    'coin', 'flip',

    # keywords
    'scry', 'storm', 'flash', 'equip',
    'retrace', 'affinity', 'totem', 'amass',

    # zones
    'outside', 'battlefield', 'exile', 'graveyard', 'library', 'hand',
    'bottom',

    # phases
    'combat', 'turn', 'draw', 'upkeep', 'noncombat',

    # creature buffs
    'menace', 'indestructible', 'trample',
    'vigilance', 'lifelink',  'first', 'double', 'strike',
    'deathtouch', 'hexproof', 'haste', 'flying', 'forestwalk',

    # damage related
    'deal', 'damage', 'fight', 'block', 'die', 'attack',
    'prevent', 'power', 'toughness', 'source', 'assign',
    'strength',

    # go wide
    'distribute', 'whenever', 'additional', 'divide',  'each', 'extra',

    # tier 1
    'sacrifice', 'tap', 'untap', 'destroy', 'target',
    'card', 'discard', 'counter', 'return', 'become',
    'choose', 'shuffle',

    # tier 2
    'life', 'total', 'mana', 'enter',  'gain', 'control',
    '+1/+1', 'pile', 'direction', 'rebound', 'resolve',

    # tier 3
    'put', 'get', 'create', 'rest', 'instead',
    'loyalty', 'number', 'onto', 'reveal',
    'equal', 'random', 'take',
]
