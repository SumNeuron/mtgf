import pandas as pd, numpy as np, seaborn as sns, matplotlib.pyplot as plt

def filter_df_for_planeswalker(df, planeswalker):
    '''
    Helper function for more semantic code.

    Arguments:
        df (pd.DataFrame): a pandas DataFrame on the list of Card Objects
            returned from the Scryfall API.
        planeswalker (str): the name (subtype) of a Planeswalker. This should
            be capitalized e.g. `'Ajani'`, `'Jace'`, etc
    Returns:
        df (pd.DataFrame): a pandas DataFrame on the list of Card Objects
            returned from the Scryfall API after being filtered for the
            specified planeswalker.
    '''
    return df[df.type_line.str.contains(planeswalker)]


def planeswalker_names_from_type(df):
    '''
    Notes:
        - assumes the DataFrame consists of only Planeswalker cards.
    Arguments:
        df (pd.DataFrame): a pandas DataFrame on the list of Card Objects
            returned from the Scryfall API.
    Returns:
        tallies (list): a list of tuples `(word, count)`, that occur in `df`
    '''
    types = df.type_line.unique()
    ignore_flips = filter(lambda t: '//' not in t, types)
    just_names = map(lambda t: t.split('— ')[-1], ignore_flips)

    # this appears to be the Planeswalker "The Wanderer", which is easier
    # to simply exclude.
    just_names = filter(lambda t: t != 'Legendary Planeswalker', just_names)

    return list(just_names)
