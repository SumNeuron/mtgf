import pandas as pd, numpy as np, seaborn as sns, matplotlib.pyplot as plt

def word_occurrence_heatmap(coo_df):
    '''
    Arguments:
        coo_df (pd.DataFrame): a pandas DataFrame in coordinate format
            containing three columns:
                - `name` (str): name of the card
                - `word` (str): what the word is
                - `state` (bool): whether the given card has the word in its
                    oracle text
    Returns:
        (f, ax, plt), the figure, axis, and plot objects from `matplotlib`
    '''
    pivot = pd.pivot_table(
        coo_df,
        values='state', index=['word'], columns=['name'], margins=True
    )
    sns_df = pivot.sort_values(by=('All'), ascending=False, inplace=False).drop('All', 1, inplace=False)
    plt.figure()
    f, ax = plt.subplots(figsize=(9, 6))
    sns.heatmap(sns_df.drop('All'), fmt="d", linewidths=.5, ax=ax)
    ax.set_xticklabels(ax.get_xticklabels(), rotation=30, ha='right')
    return f, ax, plt
