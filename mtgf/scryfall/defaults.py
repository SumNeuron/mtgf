BASE_URL = 'https://api.scryfall.com/'

# Scryfall requests that a 50-100 ms request be added between calls
GOOD_CITIZEN_RATE = 0.075
