import requests, time
from mtgf.scryfall.defaults import BASE_URL, GOOD_CITIZEN_RATE

# See Scryfall API documentation for more details: https://scryfall.com/docs/api

def search_all(params:dict={}):
    '''
    Simply requests all pages of data from Scryfall (following good citizen
    rates) for the given query.

    Arguments:
        params (dict): the params to be passed to passed to python's `requests`
            library for the Scryfall `cards/search` endpoint.

    Returns:
        results (list): a list of Scryfall card objects.
    '''
    url = '{}/cards/search'.format(BASE_URL)
    has_more = True

    results = []

    while has_more:
        res = requests.get(url, params=params)
        data = res.json()
        results.extend(data['data'])
        has_more = data['has_more']
        if has_more:
            url = data['next_page']
            time.sleep(GOOD_CITIZEN_RATE)
    return results

def fetch_catalog(catalog_name:str):
    url = '{}catalog/{}'.format(BASE_URL, catalog_name)
    res = requests.get(url)
    return res.json()

def fetch_symbology():
    url = '{}symbology'.format(BASE_URL)
    res = requests.get(url)
    return res.json()

def parse_mana(mana:str):
    url = '{}symbology/parse-mana'.format(BASE_URL)
    res = requests.get(url, params={'cost': mana})
    return res.json()

def autocomplete(query:str):
    url = '{}cards/autocomplete'.format(BASE_URL)
    res = requests.get(url, params={'q': query})
    return res.json()

def fetch_card_by_name(name:str):
    url = '{}cards/named'.format(BASE_URL)
    res = requests.get(url, params={'exact': name})
    return res.json()
