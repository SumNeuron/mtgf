__name__        = 'mtgf'
__major_version__ = 0
__minor_version__ = 0
__patch_version__ = 0
__version__     = '{}.{}.{}'.format(__major_version__, __minor_version__, __patch_version__)
__author__      = 'SumNeuron'
__description__ = 'MtG Goldfish jupyter notebooks'
__url__         = 'https://pypi.org/project/{}'.format(__name__)

__all__ = [
    'scryfall',
]
