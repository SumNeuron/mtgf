import setuptools
from mtgf import (
    __name__, __version__, __author__,
    __description__, __url__
)

with open('README.md', 'r') as fh:
    long_description = fh.read()

setuptools.setup(
    name             = __name__,
    version          = __version__,
    author           = __author__,
    author_email     = '',
    description      = __description__,
    long_description = long_description,
    long_description_content_type='text/markdown',
    url              = __url__,
    packages         = setuptools.find_packages(),
    classifiers      = [
        'Programming Language :: Python :: 3.5',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
    ],
)
