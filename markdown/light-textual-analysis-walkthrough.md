This is an accompanying text for the [article][article] on MtG Goldfish titled:
_"Brewing in Oathbreaker: is overlapping design space a problem?"_

# Walkthrough

If the text processing outlined under [Light Textual Analysis][anchor]  still
seems a bit esoteric, do not fret. Let us walk through a hypothetical example,
with "Example, the Planeswalker" to see exactly
how we manipulate its oracle text.

## Step 2 (extract oracle text):
This is just some made up text which might constitute the orcale text of
"Example, the Planeswalker"

```  
+1: Whenever a player discards a card, you may draw two cards. If you do, discard a card.
-4: Any number of target players each draw two cards.
```

## Step 3 (parse into words):
```python
[
  "+1", "whenever", "a", "player", "discards", "a", "card", "you", "may",
  "draw", "two", "cards", "if", "you", "do", "discard", "a", "card", "−4",
  "any", "number", "of", "target", "players", "each", "draw", "two", "cards"
]
```

## Step 4 (filter words):
Here we are removing common words like "a" and "the" as well as words that do not
really inform us as to what the Planeswalker does.

```python
[
  "whenever", "player", "discards", "card", "draw", "cards", "discard",
  "card", "target", "players", "each", "draw", "cards"
]
```

## Step 5 (lemmatization):
Here we convert the words to their base form.

```python
[
  "whenever", "player", "discard", "card", "draw", "card", "discard",
  "card", "target", "player", "each", "draw", "card"
]
```

## Step 6 (remove duplicates):
We are only concerned if the word appears on the Planeswalker _anywhere_, not
how many times.

```python
{
  "whenever", "player", "discard", "card", "draw", "target", "each"
}
```

## Step 7 (associate words with Planeswalker):
```python
[
  (
    "Example, the Planeswalker",
    {"whenever", "player", "discard", "card", "draw", "target", "each"}
  )
]
```

## Step 8 (tally word occurrences):
```python
[
  ("whenever", 1),
  ("player", 1),
  ("discard", 1),
  ("card", 1),
  ("draw", 1),
  ("target", 1),
  ("each", 1)
]
```

Notice that step 7 and step 8 are fairly boring in this case as we only have
one variant of our hypothetical "Example, the Planeswalker".

Suppose we had two variants of our example Planeswalker, which at step 7 would
yield:

```python
[
  (
    "Example, the Planeswalker",
    {"whenever", "player", "discard", "card", "draw", "target", "each"}
  ),
  (
    "Example, an Oathbreaker",
    {"draw", "card", "each", "player", "+1/+1", "counter", "target", "creature", "control"}
  )
]
```

Then step 8 would result in:

```python
[
  ("whenever", 1),
  ("player", 2),
  ("discard", 1),
  ("card", 2),
  ("draw", 2),
  ("target", 2),
  ("each", 2),
  ("+1/+1", 1),
  ("counter", 1),
  ("creature", 1),
  ("control", 1)
]
```
From which we can already see that of our two example Planeswalker variants,
"card" and "draw" are shared among them.

I hope this helps shed light into what exactly occurs in our textual analysis.


[article]: https://www.mtggoldfish.com/articles/brewing-in-oathbreaker-is-overlapping-design-space-a-problem
[anchor]: https://www.mtggoldfish.com/articles/brewing-in-oathbreaker-is-overlapping-design-space-a-problem#Light%20Textual%20Analysis
